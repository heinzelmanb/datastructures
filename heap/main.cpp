/**
 * Test file for priority queue
 *
 */


#include <iostream>
#include <cstdlib>
#include <time.h>

#include "Heap.hpp"
#include "Queue.hpp"



int compare(int, int);
void printArray(int* array, int len);


int main() {
  //seed random
  std::cout << "Heap test\n";
  srand(time(NULL));
  int* array = new int[10];

  for (int i = 0; i < 10; i++) {
    int rval = rand() % 200;
    array[i] = rval;
  }   

  std::cout << "Before Sort: \n";
  printArray(array, 10);

  Heap<int>::heapsort(array, 10, compare);
  
  std::cout << "After Sort: \n";
  printArray(array, 10);
  delete[] array;

  std::cout << "Queue test\n";
  Queue<int> q;
  for (int i = 0; i < 10; i++) 
    q.enqueue(i);


  for (int i = 0; i < 10; i++) 
    std::cout << q.dequeue() << std::endl;


}



int compare(int a, int b) { 
  if (a == b) return 0; 
  if (a > b) return 1;
  return -1;
}


void printArray(int* array, int len) {
  for (int i = 0; i < len; i++) 
  std::cout << array[i] << std::endl;
}


