#ifndef QUEUE_H
#define QUEUE_H

#include "IQueue.h"
#include "Node.hpp"
#include <exception>
#include <iostream>

template<typename T>
class Queue : public IQueue<T> {
  public:
    Queue() {
      head = NULL;
      tail = NULL;
    }

    ~Queue() {
      while (this->size()) {
        dequeue();
      }
    }

    void enqueue(const T item) {
      this->queueSize++;

      if (head == NULL) {
        head = new ListNode<T>(item, NULL);
        tail = head;
      } 
      else {
        ListNode<T>* newTail = new ListNode<T>(item, NULL);
        tail->setNext(newTail);
        tail = newTail;
      }
    }

    T dequeue() {
      if (!head) {
        std::cerr << "Empty Queue" << std::endl;
        throw std::exception();
      }

      ListNode<T>* tmp = head;
      T popped = head->getItem();

      head = head->getNext();

      delete tmp;
      
      this->queueSize--;

      return popped;

    }
  
    T peek() const { 
      if (!head) {
        std::cerr << "Empty Queue" << std::endl;
        throw std::exception();
      }
      return head->getItem();
    }

  private:
    ListNode<T>* head;
    ListNode<T>* tail;
  
};
#endif
