#ifndef HEAP_H
#define HEAP_H //headers
#include <exception>
#include <iostream>
#include <algorithm>
#include <queue>

#include "IQueue.h"
#include "Node.hpp"

//constants
#define LEFT 1
#define RIGHT 0 
#define NEITHER -1

template<typename T>
class Heap : public IQueue<T> {
    public:
        Heap(int (*cmp)(T,T)) {
            root = NULL; 

            this->cmp = cmp;
        }

        ~Heap() {
            delete root;
        }
        
        /**
         * Adds a new item to the heap
         * @Param: item of type T
         */
        void enqueue(const T item) {
            if (!root) {
                root = new Node<T>(item);
            } 
            else {
                enqueue(item, root);
            }
            this->queueSize++;
        }
        
        /**
         * Removes top item from the heap
         * @Return: Item contained in root node
         */
        T dequeue() {
            // throw exception if heap is empty
            if (root == NULL) { 
                std::cerr << "Heap empty\n"; 
                throw std::exception();
            }

            this->queueSize--;

            T item = root->getItem();
            
            // check if there is one item in the heap
            if (!root->getLeft() && !root->getRight()) {
                delete root;

                root = NULL;

                return item;
            }
              
            Node<T>* newRoot = getLastNode(root);

            // set the new root
            root->setItem(newRoot->getItem());

            delete newRoot;
 
            // restructure heap
            siftDown(root);
            
            return item;
        }
        
        /**
         * Gets the top value of the heap
         * @Return: returns the root node's value
         */
        T peek() const {
            if (root == NULL) {
                std::cerr << "Heap empty\n";
                throw std::exception();
            }
            return root->getItem();
        }
        
        
        /**
         * Given an array, and length
         * performs heapsort inplace
         */
        static void heapsort(T* array, int length, int(*cmp)(T,T)) {
            Heap hp = Heap(cmp);
            for (int i = 0; i < length; i++) {
              hp.enqueue(array[i]);
            }
            int i = 0;
            while(hp.size()) {
                array[i++] = hp.dequeue();    
            }
        }

    private:
        // root node of the heap
        Node<T>* root; 
		
        // user supplied comparator 
        int (*cmp)(T, T);
        
        /**
         * Recursive method to insert items into the heap.
         * @Param: item to insert, subtree to recurse on
         */
        void enqueue(const T item, Node<T>* node) {
            // the subtree with the lesser height
            int minSub = minSubtree(node);

            if (minSub == LEFT) {
                if (node->getLeft() == NULL) {
                    node->setLeft(new Node<T>(item));

                } 
                else {
                    enqueue(item, node->getLeft());
                }
            }
            else {
                if (node->getRight() == NULL) {
                    node->setRight(new Node<T>(item));

                } else {
                    enqueue(item, node->getRight());
                }
            }
            
            // sift up
            T currentItem = node->getItem();
            
            if (node->getLeft() != NULL)
                if (this->cmp(node->getLeft()->getItem(), currentItem) == 1) {
                   //swap vals
                   node->setItem(node->getLeft()->getItem());
                   node->getLeft()->setItem(currentItem);

                   //reset current item to new tree root
                   currentItem = node->getItem();
                }
            if (node->getRight() != NULL)
                if (this->cmp(node->getRight()->getItem(), currentItem) == 1) {
                   //swap vals
                   node->setItem(node->getRight()->getItem());
                   node->getRight()->setItem(currentItem);
                }
        }
        
        /**
         * Given a Node, function will return the direction of the subtree with the 
         * smaller height.
         * @Param: The tree
         */
        int minSubtree(Node<T>* tree) const {
            int leftHeight = treeHeight(tree->getLeft());

            int rightHeight = treeHeight(tree->getRight());

            return (leftHeight < rightHeight) ? LEFT : RIGHT;
        }
        
        /**
         * Returns the height of the subtree.
         * @Param: the subtree
         */
        int treeHeight(Node<T>* tree) const {
            if (tree == NULL)
                return 0;
            return std::max(treeHeight(tree->getLeft()) + 1, treeHeight(tree->getRight()) + 1); 
        }

        /**
         * Returns the last elem in the tree
         * and removes it from its current spot in the tree
         */
        Node<T>* getLastNode(Node<T>* tree) {
            // we want the max subtree rather than the min so this is reversed
            if (minSubtree(tree) == LEFT) {
                if (treeHeight(tree) == 2) {
                    Node<T>* right = tree->getRight();

                    tree->setRight(NULL);
                    return right;
                }
                return getLastNode(tree->getRight());
            } 
            else {
                if (treeHeight(tree) == 2) {
                    Node<T>* left = tree->getLeft();

                    tree->setLeft(NULL);

                    return left;
                }
                return getLastNode(tree->getLeft());
            }
            
        }

        /**
         *
         */
        int getMaxChild(Node<T>* tree) {
            // empty subtrees
            if (!tree->getLeft() && !tree->getRight()) {
                return NEITHER;
            }
            
            //right null
            if (tree->getLeft() && !tree->getRight()) {
                return LEFT;
            }

            //left null
            if (!tree->getLeft() && tree->getRight()) {
                return RIGHT;
            }
            
            //neither null
            int comparison = this->cmp(tree->getRight()->getItem(), tree->getLeft()->getItem());

            return (comparison == 1) ? RIGHT : LEFT; 
        }
        
        /**
         * Restructures a subtree after a heappop
         * @Param: a subtree
         */
        void siftDown(Node<T>* tree) {
            int maxChildDir = getMaxChild(tree);
            //subtrees are empty
            if (maxChildDir == NEITHER) {
                return;
            }

            Node<T>* maxChild = (maxChildDir == LEFT) ? tree->getLeft() : tree->getRight();

            T currentRoot = tree->getItem();
            
            // if root has lower priority than the max subtree 
            if (this->cmp(maxChild->getItem(), currentRoot) == 1) {
                tree->setItem(maxChild->getItem());
                if (maxChildDir == LEFT) {
                    tree->getLeft()->setItem(currentRoot);
                    siftDown(tree->getLeft());
                } 
                else {
                    tree->getRight()->setItem(currentRoot); 
                    siftDown(tree->getRight());
                }
            }
 
        }
        
};


#endif
