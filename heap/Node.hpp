#ifndef NODE_H
#define NODE_H

template<typename T>
class Node {
  public:
    Node(T item, Node<T> left, Node<T> right) : 
      item(item), left(left), right(right)
    {
    }

    Node(T item) : item(item), left(NULL), right(NULL)
    {
    }

    ~Node() {
      delete left;
      delete right;
    }

    T getItem() const {
      return item;
    }

    void setItem(T item) {
      this->item = item;
    }

    Node<T>* getLeft() const {
      return left;
    }

    void setLeft(Node<T>* left) {
      this->left = left;
    }

    Node<T>* getRight() const {
      return right;
    }

    void setRight(Node<T>* right) {
      this->right = right;
    }

  private:
    Node<T>* left;

    Node<T>* right;

    T item;
};

template<typename T>
class ListNode {
  public:
    ListNode(T item, ListNode<T>* next) : item(item), next(next) 
    {
    }

    T getItem() const {
      return item;
    }

    ListNode<T>* getNext() const {
      return next;
    }

    void setItem(T newItem) {
      item = newItem;
    }

    void setNext(ListNode<T>* newNext) {
      next = newNext;
    }

  private:
    ListNode<T>* next;

    T item;
};

#endif
