#ifndef I_QUEUE_H
#define I_QUEUE_H

template<typename T>
class IQueue {
	public:
		IQueue() {
			this->queueSize = 0;
		}

		virtual void enqueue(const T item) = 0;

		virtual T dequeue() = 0;

		virtual T peek() const = 0;

		int size() {
			return queueSize;
		}

	protected:
		int queueSize;
};

#endif
