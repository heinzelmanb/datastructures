#ifndef GRAPH_H
#define GRAPH_H 
/**
 * Graph data structure * much wow. So amaze
 */

//libs
#include <iostream>
#include <vector>
#include <unordered_set>
#include <map>
#include <climits>

#include "../heap/Heap.hpp"
#include "../heap/Queue.hpp"


namespace Graph {

//forward decls
template<typename T>
struct Edge;

/**
 * Node structure 
 */
template<typename T>
class Node {
  public:

    /**
     * Default
     */
    Node() {}  

    /**
     * Copy
     */
    Node(Node<T>*& other) { 
	  copy(other);
    }
 
    
    /**
     * Function to dealloc Graph
     */
    static void dealloc(Node<T>*& root) {
      Queue<Node<T>*> q;
      std::unordered_set<Node<T>*> seen;

      q.enqueue(root);
      seen.insert(root);
      
      // use a breadth first search to find all the distinct // nodes in the graph. Use the set to keep track of the
      // ones we have aleady visited. 
      while (q.size()) {
          Node<T>* top = q.dequeue();
          //q.pop();

          for (int i = 0; i < top->edges.size(); i++) {
              Node<T>* neighbor = top->edges[i].node;

              auto in = seen.find(neighbor);

              if (in == seen.end()) {
                  q.enqueue(neighbor);
                  seen.insert(neighbor);
              }
          }
      }
      
      // dealloc our nodes
      for (auto it = seen.begin(); it != seen.end(); ++it) {
          Node<T>* tmp = *it;
          delete tmp;
      }
      root = NULL;
    }
    
    /**
     * Returns the number of vertices in the graph
     */
    unsigned int size() const {
      Queue<Node<T>*> q;
      std::unordered_set<Node<T>*> seen;
      
      q.enqueue((Node<T>*)this);

      unsigned int count = 0;

      while (q.size()) {
        auto top = q.dequeue();

        if (seen.find(top) == seen.end()) {
          count++;

          for (Edge<T> e : top->edges) {
            q.enqueue(e.node);
          }
          seen.insert(top);
        }
      }
      return count;
    }
    
    /**
     * returns an array of the nodes in the graph
     */
    Node<T>** getNodes() const {
      Node<T>** nodes = new Node<T>*[this->size()];

      Queue<Node<T>*> q;
      std::unordered_set<Node<T>*> seen;
      
      q.enqueue((Node<T>*)this);
      seen.insert((Node<T>*)this);

      while(q.size()) {
        Node<T>* top = q.dequeue();

        for (int i = 0; i < top->edges.size(); i++) {
          Node<T>* neighbor = top->edges[i].node;

          if (seen.find(neighbor) == seen.end()) {
            seen.insert(neighbor);
            q.enqueue(neighbor);
          }
        }
      }
    
      int idx = 0;
      for (auto it = seen.begin(); it != seen.end(); ++it) {
        nodes[idx++] = *it;
      }
      return nodes;
    }
 
    /**
     * Performs dijkstra's shortest path algorithm
     */
    static int shortestPath(Node<T>* first, Node<T>* second) {
      unsigned int graphSize = first->size();

      Node<T>** nodes = first->getNodes();
      std::map<Node<T>*, int> idxs;

      int table[graphSize][graphSize];
      
      // create lut for NODE to index
      for (int i = 0; i < first->size(); i++)
        idxs[nodes[i]] = i;

      //set the initial weights
      for (int i = 0; i < first->size(); i++) {
        for (int j = 0; j < first->size(); j++) {
          if (i == j) {
            table[i][j] = 0;      
          } 
          else {
            table[i][j] = INT_MAX;      
          }
        }
      }


      delete[] nodes;

      return 0;
    }

    /**
     * Item to be held in each
     * Node in the graph
     */
    T item;
      
    /**
     * List of edges
     */
    std::vector<Edge<T> > edges;

  private:
    
    /**
     * Given a graph, cpy it
     */
    void copy(Node<T>* other) {
      //Node<T>* newGraph = new Node<T>();
      Node<T>* newGraph = this;
      
      //queues to hold the nodes that we want to visit
      Queue<Node<T>*> newQ;
      Queue<Node<T>*> oldQ;

      //to hold the nodes that we have already seen
	  std::map<Node<T>*, Node<T>*> seen;

      //initialize
      newQ.enqueue(newGraph);
      oldQ.enqueue(other);
      newGraph->item = T(other->item);
      seen[other] = newGraph; 

      while(oldQ.size()) {
        Node<T>* newCur = newQ.dequeue();
        Node<T>* oldCur = oldQ.dequeue();

        for (int i = 0; i < oldCur->edges.size(); i++) {
          Node<T>* old = oldCur->edges[i].node;
          Node<T>* cpy = NULL;

          if (seen.find(old) == seen.end()) {
            cpy = new Node<T>();
            cpy->item = T(old->item);

            newQ.enqueue(cpy);
            oldQ.enqueue(old);

            seen[old] = cpy;
        }
        else {
          cpy = seen[old]; 
        }

        Edge<T> e;
        e.node = cpy;

        newCur->edges.push_back(e);
        seen[oldCur] = newCur;
      }
    }
  }
};

/**
 * Edge structure
 */
template<typename T>
struct Edge {

  Edge() {
    weight = 0;
    node = NULL;
  }

  int weight;
  Node<T>* node;
};


/**
 * Overload bitshift so we can
 * print graph more easily
 */
template<typename T>
std::ostream& operator<<(std::ostream& stream, Node<T>* g) {
  std::unordered_set<Node<T>*> seen;
  Queue<Node<T>*> q;

  q.enqueue(g);

  while (q.size()) {
    Node<T>* top = q.dequeue();

    if (seen.find(top) == seen.end()) {
      stream << "Node " << top->item << " ";
      for (Edge<T> e : top->edges) {
        stream << e.node->item << " ";
        q.enqueue(e.node);
      }
      stream << std::endl;
      seen.insert(top);
    }
  }
}



/**
 * Will build a graph where each vertice 
 * is connected to every other vertice
 */
template<typename T>
Node<T>* buildKN(unsigned int k) {
  if (k <= 1) 
    return NULL;

  Node<T>** nodes = new Node<T>*[k];
  
  // alloc all our nodes
  for (int i = 0; i < k; i++) {
    Node<T>* newNode = new Node<T>();
    newNode->item = i;
    nodes[i] = newNode; 
  }

  for (int i = 0; i < k; i++) {
    Node<T>* node1 = nodes[i];
    for (int j = 0; j < k; j++) {
      // dont want an edge between a node and itself.
      if (i == j) {
        continue;
      }
      
      Node<T>* node2 = nodes[j];

      Edge<T> e;
      e.node = node2;
    
      node1->edges.push_back(e);
    }
  }
  
  auto root = nodes[0];

  delete[] nodes;
  
  return root;
}



}// end namespace
#endif
