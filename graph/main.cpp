/** 
 * Test file for graph
 * data structure
 */

#include <iostream>
#include <unordered_set>
#include <queue>
#include "graph.hpp"



int main(int argc, char** argv) {
  Graph::Node<int>* k3 = Graph::buildKN<int>(5);
  Graph::Node<int>* cpy = new Graph::Node<int>(k3);
  
  std::cout << Graph::Node<int>::shortestPath(k3, k3->edges[0].node) << std::endl;

  Graph::Node<int>::dealloc(k3);
  Graph::Node<int>::dealloc(cpy);
}

